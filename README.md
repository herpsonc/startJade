# startJade

This project's goal is to : 
1.  give you a way to launch your JADE program without using the jade's GUI.
2.  give you a set of standard algorithms for message-passing, auction, consensus and information broadcast/aggregation 

For a detailed presentation, see the showcase website : [https://startjade.gitlab.io/](https://startjade.gitlab.io/)

## Download the project

 - Using git : git clone https://gitlab.com/startjade/startJade.git (or through your ide)

 - Or downloading the archive from this page : https://gitlab.com/startjade/startJade

## Install and execute it

See the [website]((https://startjade.gitlab.io/page/startjade/presentation/)) for installation and execution details


## License

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the GNU Lesser General Public License, either version 3 or any later version.
If you want to contribute by adding a given protocol in the project you are more than welcome to do so.
Just clone the repo, add your algorithm proposal and ask for a merge-request (note that we will soon release a standard format for auction bid, so do not hesite to reach us on discord).

> Cédric Herpson, version : 19 June 2021.

